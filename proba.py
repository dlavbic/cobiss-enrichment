# -*- coding: utf-8 -*-
import lxml
import requests
import pysolr
import nltk, re, pprint

#from difflib import SequenceMatcher
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
from lxml import html
from lxml import etree
#import all datasets, models and taggers from NLTK
#nltk.download('all')

types = ['članek - sestavni del', 'zvočna kaseta', 'knjiga', 'izbrano delo', 'razne literarne vrste', 'drama', 'kratka proza']

'''join records representing the same idea in one work cluster, first based on the uniform title'''
query = 'makarovič'

payload = {'srch': 'makarovič', 'base': '99999', 'command':'SEARCH'}
cookie = {'NSC_XXX.DPCJTT.TJ': 'ffffffffc20a606a45525d5f4f58455e445a4a423660'}
r = requests.post("http://www.cobiss.si/scripts/cobiss", data=payload)
r.encoding = 'utf-8'
tree = html.fromstring(r.text)

link = tree.xpath('//td[@class="small vecje"]/a[@class="wt"]/@href')[0]
session_id = link[51:67]
server_number = link[13]
"Stevilka streznika je {0}".format(server_number)

number_of_hits = int(tree.xpath('//div[@class="row928"]/div[@class="left"]/b/text()')[0])
#the last part of the link is missing and will be appended in the for loop
link_partial = "http://cobiss" + server_number + ".izum.si/scripts/cobiss?ukaz=DISP&id=" + session_id + "&rec="

'''we save cobiss ids of the metadata records'''
cobiss_ids = []

#scraping part
'''inspect all the hits - jump into the record and extract all the data; first the data is '''
for x in range(1, number_of_hits):
    print(x)
    rec_link  = link_partial + str(x) + "&sid=1"
    req = requests.get(rec_link)
    text = html.fromstring(req.text)
    
    '''-------------------AUTHORS AND OTHER ENTITIES THAT CONTRIBUTED TO THE CREATION---------------------'''
    contributors = text.xpath('//table[@id="nolist-full"]/tbody/tr/th[text()="Avtor"]/../td/a/text()')
    '''list of contributors will be needed later; scraping does not give us information what kind of role single contributor has, but we might determine the role 
    by inspecting responsibility part in the title or by inspecting notes (opombe) or from some other source (e.g. WorldCat)'''
    contributors_list = []
    #print 'Stevilo avtorjev, dela, ki ima id {0} je {1}'.format(x, number_authors)
    for y in range(0, len(contributors)):
        print (contributors[y])
        contributors_list.append(contributors[y])
        #print 'Avtor dela, ki nosi id = {0} je {1}'.format(str(x), author_uni)

    
    '''--------------------------------------------------TITLES---------------------------------------------'''
    titles = text.xpath('//table[@id="nolist-full"]/tbody/tr/th[text()="Naslov"]/../td/text()')
    for z in range(0, len(titles)):
        title_uni = (titles[z]).encode('utf-8')
        '''titles in the cobiss system are composed of the actual title as written on the resource, followed by character '/' and further with the statement of the 
        responsibility. Examples: "Gospa Judit / Ivan Cankar" or "Kralj na Betajnovi ; Na klancu ; Črtice : (1902) / Ivan Cankar ; [uvod in opombe napisal Izidor Cankar]"'''
        #according to above explanation we split title to the actual title part and responsibility part which will be later subject to the entity and relationship extraction
        split_index = title_uni.find('/')
        if split_index != -1:
            title = title_uni[0:split_index]
            '''entity and relationship extraction of the responsibility part; problem is that all the contributors are listed as other authors ("Ostali avtorji")
            as a part of the bibliographic description (and we saved them in the list - contributors_list), but their actual responsibility is not given 
            (they can be translators, drawers, composers, etc.). That's why we try to see if contributors name appers in the 
            responsibility part of the title and if it does, we try to figure out if it is (contributors name) preceeded or followed by relation (role), such as in this example:
            /////////////////////////////////
            Kurent : starodavna pripovedka / Ivan Cankar ; z ilustracijami Božidarja Jakca ; [spremno študijo "Ivan Cankar in njegov Kurent" je napisala Vera Remic-Jager ; 
            opombo k delu B. Jakca je napisal Emilijan Cevc ; besedila k ilustracijam prvega poglavja Kurenta je izpisal Rudolf del Cott]
            ///////////////////////////////
            in the above example actual title (Kurent : starodavna pripovedka) is followed by the responsibility part, where the roles of the people are recorded in the text
            So, besides the actual author (Ivan Cankar), the roles of other contributors (Božidar Jakac, Vera Remic-Jager, Emiljan Cevc in Rudolf del Cott) are given 
            in the text and hopefull they can be extracted.
            '''
            responsibility_part = title_uni[split_index+1:]
            responsibility_part_uni = responsibility_part.decode('string_escape')
            for cl in contributors_list:
                match_ratio = fuzz.partial_ratio(cl, responsibility_part)
                #print "Primerjamo {0} z {1} in dobimo stopnjo ujemanja {2}".format(cl, responsibility_part, match_ratio)
                print (process.extract(responsibility_part_uni, contributors_list, limit=2))
                matching_treeshold = 0,75
                '''if matching ration is high, we can presume that the name of the contributor appears in the responsibility part of the title and we move on to the
                relation (role) extraction'''
                if (matching_ratio > matching_treeshold):
                    '''TO-DO relationship extraction'''

                '''sentences = nltk.sent_tokenize(responsibility_part) [1]
                sentences = [nltk.word_tokenize(sent) for sent in sentences] [2]
                sentences = [nltk.pos_tag(sent) for sent in sentences]
                print sentences'''

    uniform_title_node = text.xpath('//table[@id="nolist-full"]/tbody/tr/th[text()="Enotni naslov"]/../td/text()')
    if (len(uniform_title_node)>0):
        uniform_title = uniform_title_node[0]
    
    '''--------------------------------------------LANGUAGE-----------------------------------------------'''
    language = text.xpath('//table[@id="nolist-full"]/tbody/tr/th[text()="Jezik"]/../td/text()')
    
    '''--------------------------------------INSPECTING CONTENT--------------------------------------------'''
    physical_description_node = text.xpath('//table[@id="nolist-full"]/tbody/tr/th[contains(., "opis")]/../td/text()')
    if (len(physical_description_node)>0):
        '''checking wheter bibliographic record describes more than one book - for example Zbrano delo
        this is checked by inspecting block "Fizični opis" where the information how many volumes (število zvezkov) are there, is recorded'''
        #in the case below the book is in more than one volume
        physical_description = physical_description_node[0]
        if (physical_description.find("zv")):
            print ("Knjiga je v vec zvezkih")
    
    content_node = text.xpath('//table[@id="nolist-full"]/tbody/tr/th[text()="Vsebina"]/../td/text()')
    if (len(content_node)>0):
        '''checking whether book contains more than one work - works listed are usually separated by character ";", but due to the inconsistent cataloguing practicet they
        can as well be separated by "." (dot) or by html new line (break) character "<br>"'''
        content_list = []
        string_to_inspect = content_node[0]

        '''below case means that works are separated by new line character'''
        if (len(content_node)>1):
            for p in range(0, len(content_node)):
                    content_list.append(content_node[p])
                    #print "////////////"
                    #print content_list[p]

        else:
            if (string_to_inspect.find(";")):
                content_list = string_to_inspect.split(";")
            else:
                '''TO-DO due to the incosistent cataloguing practice the separating character might be dot as well'''
                if (string_to_inspect.find(".")):
                    content_list = string_to_inspect.split(".")
            
        
        if (len(content_list)>0):
            for r in range(0, len(content_list)):
                print ("////////////")
                print (content_list[r])

        '''TO-DO content_list contains the list of titles of the works - each should be now conected to the work in the solr database or added if it not exists yet'''        

    
    '''-------------------------------NOTES------------------------------------------------'''
    '''notes can be valuable as well, cataloguers put all sorts of stuff in them, such as entites that are responsible for the creation, as well as notes about
    the condition of the item'''
    notes_node = text.xpath('//table[@id="nolist-full"]/tbody/tr/th[text()="Opombe"]/../td/text()')
    if (len(notes_node)>0):
        notes = notes_node[0]

   
    #if language is not None:
        #print 'Delo je v jeziku: {0}'.format(language[0])
    #print 'Naslov dela, ki ima id {0} je {1}'.format(x, title)    

#----------------------END OF FOR BLOCK------------------------------------------------

'''according to cobiss id we identify whether bibliographic records has already been saved, if not we save it'''
solr = pysolr.Solr('http://localhost:8983/solr/', timeout=10)
solr.add([
    {
        "id": cobiss_id,
        "manifestation_title": title_uni,
        "author_name": authors[0] 
    },
])
results = solr.search('banana')
print("Saw {0} result(s).".format(len(results)))

'''when all records are saved, we frbrize them, if some already have FRBR work cluster defined'''
'''if record identified with cobiss id is already part of specific FRBR work cluster (identified with Work id)'''
#author strings, title strings, possible VIAF identifier is what identify work cluster
#WORK CLUSTER = collocates all the resources derived from the same idea
print (SequenceMatcher(None, 'Apple', 'Appel').ratio())

print (server_number)