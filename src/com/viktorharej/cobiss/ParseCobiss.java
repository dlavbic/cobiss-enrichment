package com.viktorharej.cobiss;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.lavbic.web.Client;

public class ParseCobiss {
	/**
	 * @param argv
	 */
	public static void main(String[] argv) {
		
		int numberOfHits = 0;
		int numberOfIterations = 0;
		String url="";
		String naslov="";
		String avtor="";
		String jezik="";
		String vrsta="";
		//HashMap, ki hrani kot ključ številko dela v zaporedju pregledovanja in kot vrednosti arraylist 
		//vsemi podatki o knjigi, v tem vrstnem redu: avtor, naslov, vrsta, jezik 
		Map<String, ArrayList<String>> authors = new HashMap <String, ArrayList<String>>();
		Map<String, ArrayList<String>> titles = new HashMap <String, ArrayList<String>>();
		Map<String, ArrayList<String>> forms = new HashMap<String, ArrayList<String>>();
		Map<String, ArrayList<String>> languages = new HashMap<String, ArrayList<String>>();
		Map<String, ArrayList<String>> years = new HashMap<String, ArrayList<String>>();
		
		String iskalniNiz = "kosovirja";
		Client c = new Client(true, "UTF-8");
		String string = c.post("http://www.cobiss.si/scripts/cobiss", new String[][] {
				{"base", "99999"}, {"command", "SEARCH"}, {"srch", iskalniNiz}, {"x", "25"}, {"y", "10"}});
		Matcher match = Pattern.compile("<tr>[^<]*?<td [^<]*?><input [^<]*?/></td>[^<]*?<td [^<]*?>(\\d+?)\\.</td>[^<]*?<td [^<]*?><img [^<]*?/></td>[^<]*?<td [^<]*?>(.*?)</td>[^<]*?<td [^<]*?><a href=\"([^\"]*?)\">([^<]*?)</a></td>[^<]*?<td [^<]*?><img [^<]*?/>([^<]*?)</td>[^<]*?<td [^<]*?>([^<]*?)</td>[^<]*?<td [^<]*?>(\\d+?)</td>").matcher(string);
		System.out.println("Število grup: "+ match.groupCount());
		
		Matcher match0 = Pattern.compile("Število najdenih zapisov:&nbsp;<b>(\\d+)").matcher(string); 
		while (match0.find()){
			numberOfHits = Integer.parseInt(match0.group(1));
		}
		
		System.out.println("Število najdenih zadetkov: "+Integer.toString(numberOfHits));
		
		if (numberOfHits>10)
			numberOfIterations = numberOfHits/10;
		else
			numberOfIterations = 1;
		
		
		//pregledamo le prvih deset zadetkov
		while (match.find()) {
			Integer stevilo = Integer.valueOf(match.group(1));
			String avtorTemp = match.group(2).replaceAll(" <br>", "; ");
			String naslovTemp = match.group(4);
			String vrstaTemp = match.group(5);
			String jezikTemp = match.group(6);
			String letoTemp = match.group(7);
			//String cobissId = match.group(8);
			
		 System.out.println("st = {" + match.group(1) + "}, " +
					"avtor = {" + avtorTemp + "}, " +
					"naslov = {" + naslovTemp + "}, " +
					"vrsta gradiva = {" + vrstaTemp + "}, " +
					"jezik = {" + jezikTemp + "}, " +
					"leto = {" + letoTemp + "}, " +
					"urlPodrobnosti = {" + match.group(3) + "}");
			url = match.group(3);
			
			/*if (authors.containsKey(avtorTemp))
				authors.get(avtorTemp).add(cobissId);
			else {
				ArrayList<String> temp = new ArrayList<String>();
				temp.add(cobissId);
				authors.put(avtorTemp, temp);
			}
			
			if (titles.containsKey(naslovTemp))
				titles.get(naslovTemp).add(cobissId);
			else {
				ArrayList<String> temp = new ArrayList<String>();
				temp.add(cobissId);
				titles.put(naslovTemp, temp);
			}
			
			if (forms.containsKey(vrstaTemp))
				forms.get(vrstaTemp).add(cobissId);
			else {
				ArrayList<String> temp = new ArrayList<String>();
				temp.add(cobissId);
				forms.put(vrstaTemp, temp);
			}
			
			if (languages.containsKey(jezikTemp))
				languages.get(jezikTemp).add(cobissId);
			else {
				ArrayList<String> temp = new ArrayList<String>();
				temp.add(cobissId);
				languages.put(jezikTemp, temp);
			}
			
			if (years.containsKey(jezikTemp))
				years.get(letoTemp).add(cobissId);
			else {
				ArrayList<String> temp = new ArrayList<String>();
				temp.add(cobissId);
				years.put(letoTemp, temp);
			}
			
		} 
		int position = url.indexOf("id=") + 3;
		int position2 = position + 16;
		System.out.println("url = "+url);
		
		String id = url.substring(position, position2);

		int position3 = url.indexOf("cobiss")+6;
		int position4 = position3 + 1;
		String cobissNumber = url.substring(position3, position4);
		
		//pregledamo še preostale zadetke
		for (int i=1; i<=numberOfIterations;i++){
			String nextUrl = "http://cobiss"+ cobissNumber +".izum.si/scripts/cobiss?ukaz=DIRE&id=" + id +"&dfr="+ i +"1&ppg=50&sid=1";
			String string2 = c.get(nextUrl);
			Matcher match2 = Pattern.compile("<tr>[^<]*?<td [^<]*?><input [^<]*?/></td>[^<]*?<td [^<]*?>(\\d+?)\\.</td>[^<]*?<td [^<]*?><img [^<]*?/></td>[^<]*?<td [^<]*?>(.*?)</td>[^<]*?<td [^<]*?><a href=\"([^\"]*?)\">([^<]*?)</a></td>[^<]*?<td [^<]*?><img [^<]*?/>([^<]*?)</td>[^<]*?<td [^<]*?>([^<]*?)</td>[^<]*?<td [^<]*?>(\\d+?)</td>[^<]*?<div class=\"bd\">(\\d+?)\\.</div>").matcher(string2);
			while (match2.find()) {
				Integer stevilo = Integer.valueOf(match2.group(1));
				String avtorTemp = match2.group(2).replaceAll(" <br>", "; ");
				String naslovTemp = match2.group(4);
				String vrstaTemp = match2.group(5);
				String jezikTemp = match2.group(6);
				String letoTemp = match2.group(7);
				String cobissId = match2.group(8);
				
				if (authors.containsKey(avtorTemp))
					authors.get(avtorTemp).add(cobissId);
				else {
					ArrayList<String> temp = new ArrayList<String>();
					temp.add(cobissId);
					authors.put(avtorTemp, temp);
				}
				
				if (titles.containsKey(naslovTemp))
					titles.get(naslovTemp).add(cobissId);
				else {
					ArrayList<String> temp = new ArrayList<String>();
					temp.add(cobissId);
					authors.put(avtorTemp, temp);
				}
				
				
				System.out.println("st = {" + match2.group(1) + "}, " +
						"avtor = {" + avtorTemp + "}, " +
						"naslov = {" + naslovTemp + "}, " +
						"vrsta gradiva = {" + vrstaTemp + "}, " +
						"jezik = {" + jezikTemp + "}, " +
						"leto = {" + letoTemp + "}, " +
						"urlPodrobnosti = {" + match2.group(3) + "}");
			} 
		}
		System.out.println("---------------------------");
		/*for (int j=1; j<=numberOfHits; j++){
			Integer cifra = new Integer(j);
			ArrayList<String> podatkiODelu = new ArrayList<String>();
			String nextUrl = "http://cobiss"+ cobissNumber +".izum.si/scripts/cobiss?ukaz=DISP&id="+ id +"&rec="+ j +"&sid=1";
			//System.out.println(nextUrl);
			String string2 = c.get(nextUrl);
			//Matcher match2 = Pattern.compile("<tr><th scope=\"row\" valign=\"top\" width=\"120\">Avtor</th><td><a href=[^<]+?>(.+)</a></td></tr>").matcher(string2);
			Matcher match2 = Pattern.compile("<tr><th scope=\"row\" valign=\"top\" width=\"120\">Avtor</th><td><a href=[^<]+?>(.+)</a></td></tr>\\s+<tr><th scope=\"row\" valign=\"top\">Naslov</th><td>\\s*+(.+)</td></tr>").matcher(string2);
			Matcher match3 = Pattern.compile("<tr><th scope=\"row\" valign=\"top\">Vrsta/vsebina</th><td><img[^<]+?>\\s*(.+)</td></tr>\\s+<tr><th scope=\"row\" valign=\"top\">Jezik</th><td>(.+)</td></tr>").matcher(string2);
			while (match2.find()) {
				avtor = match2.group(1);
				naslov = match2.group(2);
				
				System.out.println("Avtor: " + avtor);
				
				podatkiODelu.add(avtor);
				podatkiODelu.add(naslov);
				
				//dodamo v map titles kot ključ naslov dela in kot vrednost(i) zaporedno številko dela
				//tako zgradimo preprost iskalni indeks
				/*if (titles.containsKey(naslov))
				   titles.get(naslov).add(cifra); 	
				else {
				   ArrayList<Integer> list = new ArrayList<Integer>();
				   list.add(cifra);
				   titles.put(j, list); 
				}
				
				System.out.println("Naslov: " + naslov);
			} 
			while(match3.find()){
				vrsta = match3.group(1);
				jezik = match3.group(2);
				//System.out.println("Vrsta: " + vrsta);
				//System.out.println("Jezik: " + jezik);
				podatkiODelu.add(vrsta);
				podatkiODelu.add(jezik);
			}
			System.out.println("-------------------------");
		}
		*/
		/*System.out.println("Se pojavi v naslovu pri zaporednih številkah: ");
		for (Integer key : titles.keySet()) {
			System.out.println(key);
		}
		
		System.out.println("Se pojavi kot avtor: ");
		for (String key : authors.keySet()) {
			System.out.println(key);
			for (Integer value : authors.get(key))
				System.out.println(value);
		}*/
			
	}
	}
}
