package com.viktorharej.cobiss;

/**
 * Created by viktorharej on 11.9.2015.
 */
public class Contributor {
    private String birthName;
    private String commonName;
    private String alternativeNames;
    private int yearOfBirth;
    private int yearOfDeath;

    public Contributor(){

    }

    public String getCommonName(){
        return this.commonName;
    }

    public void setCommonName(String name){
        this.commonName = name;
    }
}
