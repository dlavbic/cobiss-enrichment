package com.viktorharej.cobiss;

import com.viktorharej.cobiss.Language;

/**
 * Created by viktorharej on 10.9.2015.
 */
public class Title {
    private String title;
    private Language language;
    private String type;

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Language getLanguage() {
        return this.language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }
}
