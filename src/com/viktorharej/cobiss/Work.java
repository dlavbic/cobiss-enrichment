package com.viktorharej.cobiss;

import com.viktorharej.cobiss.Contributor;
import com.viktorharej.cobiss.Title;

import java.util.HashMap;

/**
 * Created by viktorharej on 10.9.2015.
 */
public class Work {
    //only author or composer relationship can happen on the
    private HashMap<String, Contributor> contributors = new HashMap<String, Contributor>();
    private HashMap<String, Title> titles = new HashMap<String, Title>();
}
