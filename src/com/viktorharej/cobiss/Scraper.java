package com.viktorharej.cobiss;

import java.io.IOException;
import java.io.StringReader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.xml.sax.InputSource;

import net.lavbic.web.Client;


public class Scraper {

    public static void parseContributor(String searchQuery) {

    }

    public static void parseContent(String searchQuery) {

    }

    public static void parseTitle (String searchQuery) throws XPathExpressionException, SolrServerException, IOException {

        int numberOfHits = 0;
        int numberOfIterations = 0;
        int startPos = 0;
        String url = "";
        String urlId = "";
        String title = "";
        //responsibility part of the title
        String responsibilityPart = "";
        ArrayList<String> parts = new ArrayList<String>();
        HashMap<String, String> namesAndRoles = new HashMap<String, String>();
        String author = "";
        String language = "";
        String year = "";
        String type = "";
        String udk = "";
        String isbn = "";
        //whole field "ostali avtorji"
        String otherAuthors = "";
        //whole field "opombe"
        String notes = "";
        //whole field "predmetne oznake" = subject headings
        String subjectHeadings = "";

        String niz = "";
        String cobissId = "";
        String sessionId = "";
        String serverNumber = "";
        String manifestationIdentifier = "";
        //HashMap, ki hrani kot ključ številko dela v zaporedju pregledovanja in kot vrednosti arraylist
        //vsemi podatki o knjigi, v tem vrstnem redu: avtor, naslov, vrsta, jezik
        Map<String, String> authors = new HashMap<String, String>();
        Map<String, String> titles = new HashMap<String, String>();
        Map<String, String> forms = new HashMap<String, String>();
        Map<String, String> languages = new HashMap<String, String>();
        Map<String, String> years = new HashMap<String, String>();
        Map<String, String> isbns = new HashMap<String, String>();

        //here we put all the different contributors (key=role, value=contributors name) taken from responsibility part
        Map<String, String> contributors = new HashMap<String, String>();
        //here we put all the different contributors (key=cobiss id of the contributor, value=contributor's name) taken from field "ostali avtorji"
        Map<String, String> contributorsOthers = new HashMap<String, String>();
        //list of contributors and roles taken from the field "Opombe" (key=string possibly representing role, value=string possibly representing contributor's name)
        Map<String, String> contributorsNotes = new HashMap<String, String>();

        //possible work identifier, needed during the process of FRBRization
        ArrayList<String> possibleWorkIds = new ArrayList<String>();

        //=============Solr stuff===========================
        String urlString = "http://localhost:8983/solr/cobiss";
        SolrClient solr = new HttpSolrClient(urlString);
        //==================================================

        String urlRelations = "http://localhost:8983/solr/relations";
        SolrClient solrRelations = new HttpSolrClient(urlRelations);

        //String iskalniNiz = "kosovirja";
        Client c = new Client(true, "UTF-8");
        String string = c.post("http://www.cobiss.si/scripts/cobiss", new String[][]{
                {"base", "99999"}, {"command", "SEARCH"}, {"srch", searchQuery}, {"x", "25"}, {"y", "10"}});

        Matcher match = Pattern.compile("<span class=\"na-si\"><a href=([^<]*?)</a></span>").matcher(string);
        while (match.find()) {
            niz = match.group(1);
            System.out.println(niz);
        }

        //iščemo številko strežnika
        int positionCobiss = (niz.indexOf("cobiss")) + 6;
        System.out.println(positionCobiss);

        //iščemo id seje
        int positionSessionId = niz.indexOf("id=") + 3;
        System.out.println(positionSessionId);

        //iz niz-a izluščimo številko strežnika in številko seje
        serverNumber = niz.substring(positionCobiss, positionCobiss + 1);
        System.out.println("Server number: " + serverNumber);

        sessionId = niz.substring(positionSessionId, positionSessionId + 16);
        System.out.println("Session id: " + sessionId);

        //http://cobiss6.izum.si/scripts/cobiss?ukaz=DISP&id=1236047067116364&rec=1&sid=1


        Matcher matchNumberOfRecords = Pattern.compile("Število najdenih zapisov:&nbsp;<b>(\\d+)").matcher(string);
        while (matchNumberOfRecords.find()) {
            numberOfHits = Integer.parseInt(matchNumberOfRecords.group(1));
        }

        System.out.println("Število najdenih zadetkov: " + Integer.toString(numberOfHits));
        System.out.println();

        //vnaprej vstavimo številko strežnika in id seje, ki se ne bosta spreminjali med "scrapanjem"
        String nextUrlPart = "http://cobiss" + serverNumber + ".izum.si/scripts/cobiss?ukaz=DISP&id=" + sessionId;

        for (int i = 1; i <= numberOfHits; i++) {
            String nextUrl = nextUrlPart + "&rec=" + i + "&sid=1";
            String stringContent = c.get(nextUrl);
            System.out.println("Next url: " + nextUrl);


            //izluščimo trajno povezava na zapis, ki nam bo kasneje služila tudi kot identifikator
            //we also scrape for the uniquer record identifier (manifestation identifier)
            startPos = stringContent.indexOf("class=\"bd\"");
            if (startPos != -1) {
                int endPos = stringContent.indexOf("</div>", startPos + 11);
                urlId = stringContent.substring(startPos + 11, endPos);
                System.out.println("url id: " + urlId);
                int endPosIdentifier = urlId.indexOf("&", 70);
                manifestationIdentifier = urlId.substring(71, endPosIdentifier);
                System.out.println("Manifestation identifier: " + manifestationIdentifier);
            }

            //in the case, where manifestation record already exists in the database, there is no need for
            //scrapping
            SolrQuery query = new SolrQuery();
            query.setQuery(cobissId);
            query.setFields("cobissId");
            QueryResponse response = solr.query(query);
            SolrDocumentList results = response.getResults();

            //in case there is no manifestation record in the database, we start scrapping and we then insert manifestation record in the
            //solr database
            if (results.size() == 0) {
                System.out.println("noter");

                //gremo v vsak zapis posebej in iz njega luščimo podatke
                Matcher matchContent = Pattern.compile("<table id=\"nolist-full\".*?class=\"record\">.*?<tbody>.*?<tr>(.+)</tr>.*?</tbody>.*?</table>", Pattern.DOTALL).matcher(stringContent);
                while (matchContent.find()) {
                    String content = matchContent.group(1);
                    //System.out.println(content);
                    //TO-DO: we make solr query to check if the manifestation with this id was already inserted

                    //izluščimo avtorja
                    HashMap<String, String> authorData = new HashMap<String, String>();
                    startPos = content.indexOf("Avtor");
                    if (startPos != -1) {
                        /*startPos = (content.indexOf(";\">", startPos + 16));
                        int endPos = content.indexOf("</a>", startPos);
                        author = content.substring(startPos + 3, endPos);
                        System.out.println("Prvi avtor: " + author);
                        authors.put(urlId, author);*/
                        String authorUrl = "";

                        int endPos = content.indexOf("</a>", startPos);
                        author = content.substring(startPos+5,endPos);
                        if (author.contains("id")) {
                            authorUrl = author.substring(author.indexOf("id")+2);
                        }
                        String authorTerm = "";
                        if (author.contains("term")) {
                            authorTerm = author.substring(author.indexOf("term") + 4);
                        }
                        authorData.put(authorUrl, authorTerm);
                        for (Map.Entry<String, String> entry : authorData.entrySet())
                            System.out.println("avtor url: " + entry.getKey() + " , autorTerm" + entry.getValue());
                    }

                    startPos = content.indexOf("Naslov");
                    if (startPos != -1) {
                        startPos = (content.indexOf("</th><td>", startPos + 6)) + 9;
                        int endPos = content.indexOf("</td></tr>", startPos);
                        title = content.substring(startPos, endPos);
                        System.out.println("Naslov: " + title);
                        //cataloguing rules that title is followed by character "/" and then by responsibility part
                        startPos = title.indexOf("/");
                        responsibilityPart = title.substring(startPos + 2, title.length());
                        System.out.println("Responsibility part: " + responsibilityPart);

                        //since ";" separates different persons and their roles, we first split string according to ";"
                        String[] differentiated = responsibilityPart.split(";");
                        //we then search and further split each individual part for persons and roles of this person or these persons
                        for  (String section : differentiated) {
                            System.out.println("Del je: " + section);
                            StringTokenizer st = new StringTokenizer(section);
                            String word = "";
                            //System.out.println("Word: " + word);
                            StringBuilder role = new StringBuilder();
                            StringBuilder name = new StringBuilder();
                            while (st.hasMoreTokens()) {
                                word = st.nextElement().toString();
                                if (word.startsWith("[")){
                                    word = word.substring(1);
                                }
                                if (word.endsWith("]")){
                                    word = word.substring(0, word.length()-1);
                                }

                                if (word.trim().length() > 0) {
                                    if (word.equals(WordUtils.capitalize(word)) && (!(word.equals("["))) && (!(word.equals("]")))) {
                                        name.append(word + " ");
                                        System.out.println(word);
                                    } else {
                                        role.append(word + " ");
                                        System.out.println(word);
                                    }
                                }
                            }
                            namesAndRoles.put(role.toString(), name.toString());
                            //we need to insert namesAndRoles in solr database as well
                        }
                        Set<Map.Entry<String, String>> set = namesAndRoles.entrySet();

                        for (Map.Entry<String, String> me : set) {
                            System.out.println("Role :"+me.getKey() +" Name : "+ me.getValue());

                            String role = me.getKey();
                            String contributorName = me.getValue();

                            SolrInputDocument document = new SolrInputDocument();
                            document.addField("relationship", role);
                            document.addField("name", contributorName);
                            UpdateResponse solrResponse = solrRelations.add(document);


                            //here, we make the distinction for different types of contributors
                            //has author = rdau:P60434
                            if (role.contains("spisal")){
                                contributors.put("rdau:P60434", contributorName);
                            }
                            ////blok ki se tiče spremnega besedila
                            /*
                            "has writer of supplementary textual content"(rdau:P60392):
                             - "has writer of added text"(rdau:P60380),
                             - "has writer of preface"(rdau:P60389),
                             - "has writer of added commentary"(rdau:P60390),
                             - "has writer of introduction" (rdau:P60391)
                            */
                            //has writer of added commentary
                            if (role.contains("spremno besedo")){
                                contributors.put("rdau:P60390", contributorName);
                            }
                            //avtorjeva biografija je added text
                            //"has biographical information" = rdau:P60492
                            if (role.contains("biografija")){
                                //added text
                                contributors.put("rdau:P60492", contributorName);
                                //has biographical information
                                contributors.put("rdau:P60492", contributorName);
                            }
                            //"has writer of introduction"
                            if (role.contains("uvod napisal")) {
                                contributors.put("rdau:P60391", contributorName);
                            }
                            //if audio book than:
                            // has narrator = rdau:60153
                            if (role.contains("pripoveduje") || (role.contains("bere"))){
                                contributors.put("rdau:P60153", contributorName);
                            }
                            //"has arranger of music"
                            if (role.contains("aranžiral")){
                                contributors.put("rdau:P60379", contributorName);
                            }
                            //has transcriber
                            if (role.contains("traskribiral")){
                                contributors.put("rdau:P60382", contributorName);
                            }
                            //has editor = rdau:P60393
                            if (role.contains("uredil")){
                                contributors.put("rdau:P60393", contributorName);
                            }
                            //has illustrator = rdau:P60396:
                            if (role.contains("ilustracije")){
                                contributors.put("rda:P60396", contributorName);
                            }
                            if (role.contains("brosuro oblikovala in uredila")){

                            }
                            //"has photographer"
                            if (role.contains("fotograf")){
                                contributors.put("rdau:P60429", contributorName);
                            }
                            //has translator
                            if (role.contains("preved")){
                                contributors.put("rdau:P60385", contributorName);
                            }
                            //"has abridger" = rdau:P60394
                            if (role.contains("skrajšano besedilo pripr")){
                                contributors.put("rdau:P60394", contributorName);
                            }
                            //"has writer of added commentary" = rdau:P60390
                            if (role.contains("komentar napisal")){
                                contributors.put("rdau:P60390", contributorName);
                            }
                            //"has writer of introduction" = rdau:P60391
                            if (role.contains("uvod napis")){

                            }
                            if (role.contains("izbral") ||role.contains("sestavil")  ){
                                //has compiler
                                contributors.put("rdau:P60428", contributorName);
                            }
                            /*
                            HAS CONTRIBUTOR ima podrazrede:
                            - rdau:P60377 "has surveyor"
                            - rdau:P60378 "has animator"
                            - rdau:P60379 "has arranger of music"
                            - rdau:P60381 "has draftsman"
                            - rdau:P60382 "has transcriber"
                            - rdau:P60383 "has musical director"
                            - rdau:P60384 "has costume designer"
                            - rdau:P60385 "has translator"
                            - rdau:P60386 "has art director"
                            - rdau:P60387 "has performer"
                            - rdau:P60388 "has court reporter"
                            - rdau:P60392 "has writer of supplementary textual content"
                            - rdau:P60393 "has editor"
                            - rdau:P60394 "has abridger"
                            - rdau:P60395 "has recording engineer"
                            - rdau:P60396 "has illustrator"
                            - rdau:P60397 "has recordist"
                            - rdau:P60399 "has stage director"
                            - rdau:P60419 "has presenter"
                            - rdau:P60422 "has editor of moving image work"
                            - rdau:P60437 "has production designer"
                            - rdau:P60446 "has minute taker"
                            - rdae:P20053 "has contributor"
                            - rdau:P60748 "has visual effects provider"
                            - rdau:P60749 "has special effects provider"
                            - rdau:P60809 "has writer of foreword"
                            - rdau:P60811 "has sound designer"
                            - rdau:P60813 "has lighting designer"
                            - rdau:P60824 "has on-screen participant"
                            - rdau:P60828 "has software developer"
                            - rdau:P60838 "has colourist"
                            - rdau:P60840 "has make-up artist"
                            */

                            //skrajšano verzijo uredil
                            //has abridger = rdau:P60394

                            //in the opposite case we add it as it is for later analysis
                            else {
                                contributors.put(role, contributorName);
                            }

                        }

                        namesAndRoles.clear();

                        //TO-DO
                        //we will further match names in array list author and Names and Roles
                        //in this way we can match names with roles

                    }



                    //izluščimo tip vsebine
                    startPos = content.indexOf("Vrsta/vsebina");
                    if (startPos != -1) {
                        startPos = (content.indexOf("alt=\"type of material\" />", startPos + 13)) + 25;
                        int endPos = content.indexOf("</td></tr>", startPos);
                        type = content.substring(startPos, endPos);
                        System.out.println("Vrsta/vsebina: " + type);
                        forms.put(urlId, type);
                    }

                    //izluščimo jezik
                    startPos = content.indexOf("Jezik");
                    if (startPos != -1) {
                        startPos = (content.indexOf("</th><td>", startPos + 5)) + 9;
                        int endPos = content.indexOf("</td></tr>", startPos);
                        language = content.substring(startPos, endPos);
                        System.out.println("Jezik: " + language);
                        languages.put(urlId, language);
                    }

                    //izluščimo leto izida
                    startPos = content.indexOf("Leto");
                    if (startPos != -1) {
                        startPos = (content.indexOf("</th><td>", startPos + 4)) + 9;
                        int endPos = content.indexOf("</td></tr>", startPos);
                        year = content.substring(startPos, endPos);
                        System.out.println("Leto: " + year);
                        years.put(urlId, year);
                    }

                    //izluščimo UDK vrstilec
                    startPos = content.indexOf("UDK");
                    if (startPos != -1) {
                        startPos = (content.indexOf("</th><td>", startPos + 3)) + 9;
                        int endPos = content.indexOf("</td></tr>", startPos);
                        udk = content.substring(startPos, endPos);
                        System.out.println("UDK: " + udk);
                    }

                    //izluščimo ISBN številko
                    startPos = content.indexOf("ISBN");
                    if (startPos != -1) {
                        startPos = (content.indexOf("</th><td>", startPos + 3)) + 13;
                        int endPos = content.indexOf("</td></tr>", startPos);
                        isbn = (content.substring(startPos, endPos)).trim();
                        System.out.println("ISBN: " + isbn);
                        isbns.put(urlId, isbn);
                    }

                    //we scrap the notes (opombe)
                    //in the notes there can be very meaningful data
                    startPos = content.indexOf("Opombe");
                    if (startPos != -1){
                        startPos = (content.indexOf("</th><td>", startPos+5)) + 9;
                        int endPos = content.indexOf("</td></tr>", startPos);
                        //all the notes are indexed
                        notes = (content.substring(startPos, endPos)).trim();

                        String[] partsNotes = null;
                        if (notes.contains("<br")) {
                            partsNotes = notes.split("<br/>");
                            for (String partN : partsNotes){
                                System.out.println("part notes: " + partN);
                            }
                        }
                    }

                    startPos = content.indexOf("Predmetne oznake");
                    if (startPos != -1){
                        startPos = (content.indexOf("</th><td>", startPos+5)) + 9;
                        int endPos = content.indexOf("</td></tr>", startPos);
                        subjectHeadings = (content.substring(startPos, endPos)).trim();
                        String[] partsHeaders = null;
                        if (notes.contains("<br")) {
                            partsHeaders = notes.split("<br/>");
                            for (String partH : partsHeaders){
                                System.out.println("part headers: " + partH);
                            }
                        }
                    }

                    startPos = content.indexOf("Ostali avtorji");
                    if (startPos != -1){
                        startPos = (content.indexOf("</th><td>", startPos+5)) + 9;
                        int endPos = content.indexOf("</td></tr>", startPos);
                        otherAuthors = (content.substring(startPos, endPos)).trim();
                        String[] partsAuthors = null;
                        if (notes.contains("<br")) {
                            partsAuthors = notes.split("<br/>");
                            for (String partA : partsAuthors){
                                if (partA.contains(";id="))
                                System.out.println("part other authors: " + partA);
                            }
                        }
                    }

                    System.out.println();
                }

                //=============part where link names to the authoritys' idetifiers (here it where also FRBRization happens)====================

                //first we check if there is a work instance to which our manifestation can be appended
                //we first search by title amongst works
                for (Map.Entry<String, String> tit: titles.entrySet()) {

                    SolrQuery queryWork = new SolrQuery();
                    query.setQuery(tit.getValue());
                    query.setFields("workTitle", "additionalWorkTitle");
                    QueryResponse responseWork = solr.query(queryWork);
                    SolrDocumentList resultsWork = responseWork.getResults();
                    for (SolrDocument doc: resultsWork){
                        Collection<String> fieldNames = doc.getFieldNames();
                        for (String fieldName:fieldNames){
                            if (fieldName.equals("workId")){
                                possibleWorkIds.add((String) doc.getFirstValue("workId"));
                            }
                        }
                    }

                }
                //refine workId values by trying to match also author if exists

                //double checking if we appended proper roles to the contributors names
                //therefore we kept two separate maps of contributors - one parsed from the responsibily part (contributors)
                //and another taken from 9XX fields



                //===========================end of preparation for the solr=====================


                //====================== start SOLR section =====================================

                //we need to check for work record and link manifestation with a work
                //first we try to find work records which have specific author

                System.out.println("Do tuki vse ok  ");

                SolrQuery identifierQuery = new SolrQuery();
                identifierQuery.setQuery(manifestationIdentifier);
                identifierQuery.setFields("cobissId");

                QueryResponse resultIdentifier = solr.query(identifierQuery);
                //based on the identifier we know that we already
                if (results.size() > 1) {
                    System.out.println("Pojavna oblika ze obstaja");
                    break;
                }


                //finally if no work record can be matched, we create a new one
                //Work work = new Work();

                //on these records we search for the title

                //we construct manifestation object and we prepare it for the insertion into solr database,
                //including work reference
                //if needed this work reference can be latter changed
                Manifestation manifestation = new Manifestation(manifestationIdentifier);
                //the whole title part (with responsibility part) taken from field "Naslov" from Cobiss
                manifestation.setCobissTitle(title);
                manifestation.setNotes(notes);
                manifestation.setOtherAuthors(otherAuthors);
                manifestation.setSubjectHeadings(subjectHeadings);
                manifestation.setUDK(udk);
                manifestation.setISBN(isbn);
                SolrInputDocument document = manifestation.prepareForSolr();

                System.out.println("Cobiss Title: " + document.getField("cobissTitle"));
                System.out.println("Notes: " + document.getField("notes"));

                //we insert it into solr database
                UpdateResponse solrResponse = solr.add(document);
                System.out.println("Odgovor: " + solrResponse.toString());

                //next step is to link manifestation with appropriate work
                //we either link it with already existing work or we create new work
                //to check if appropriate work record already exists we need to query solr database

                //SolrQuery workQuery = new SolrQuery();
                //to check whether appropriate work record already exists we first search by the title
                //linking the manifestation with the right work is crucial part of the application
                //1. work might have the same title as manifestation
                query.setQuery(title);
                //query.setField();
                //====================== end SOLR section =======================================
            } //end if(resutls.size()>0)
            //tukaj bom podatke metal v podatkovne strukture in jih urejal za potrebe fasetne klasifikacije
            for (Map.Entry<String, String> entry : languages.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                //System.out.println("ključ: " + key + ";vrednost: " + value);
            }
        }
        //end for loop
        solr.commit();
        solrRelations.commit();
    }//end parseTitle




}
