package com.viktorharej.cobiss;

import com.viktorharej.cobiss.Contributor;
import com.viktorharej.cobiss.Place;
import com.viktorharej.cobiss.Record;
import com.viktorharej.cobiss.Title;
import org.apache.solr.common.SolrInputDocument;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by viktorharej on 10.9.2015.
 */
public class Manifestation extends Record {
    private HashMap<String, Title> titles = new HashMap<String, Title>();
    private HashMap<String, Contributor> contributors = new HashMap<String, Contributor>();
    //below array contains all the places related to manifestation, such as place of creation, etc.
    private HashMap<String, Place> places = new HashMap<String, Place>();
    private String cobissId = "";
    private String workId = "";
    private String cobissTitle = "";
    private String notes = "";
    private String subjectHeadings = "";
    private String otherAuthors = "";
    private String UDK = "";
    private String ISBN = "";

    //empty constructor
    public Manifestation(String cobissId){
        this.cobissId = cobissId;
    }

    public void setTitle(String titleRole, Title title){
        this.titles.put(titleRole, title);
    }

    public String getCobissTitle(){
        return this.cobissTitle;
    }

    public void setCobissTitle(String cobissTitle){
        this.cobissTitle = cobissTitle;
    }

    public String getCobissId(){
        return this.cobissId;
    }

    public void setCobissId() {
        this.cobissId = cobissId;
    }

    public String getWorkId(){
        return this.workId;
    }

    public void setWorkId(String workId){
        this.workId = workId;
    }

    public String getNotes(){
        return this.notes;
    }

    public void setNotes(String notes){
        this.notes = notes;
    }

    public String getSubjectHeadings(){
        return this.subjectHeadings;
    }

    public void setSubjectHeadings(String subjectHeadings){
        this.subjectHeadings = subjectHeadings;
    }

    public String getOtherAuthors(){
        return this.otherAuthors;
    }

    public void setOtherAuthors(String otherAuthors){
        this.otherAuthors = otherAuthors;
    }

    public String getUDK(){
        return this.UDK;
    }

    public void setUDK(String UDK){
        this.UDK = UDK;
    }

    public String getISBN(){
        return this.ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    //TO-DO
    //function that prepares metadata for the insertion into solr collection
    public SolrInputDocument prepareForSolr() {

        SolrInputDocument document = new SolrInputDocument();
        System.out.println("znotraj pojavne oblike");

        /*if (!contributors.isEmpty()) {
            for (Map.Entry<String, Contributor> contributor : contributors.entrySet()) {
                document.addField(contributor.getKey(), contributor.getValue());
            }
        }

        if (!titles.isEmpty()) {
            for (Map.Entry<String, Title> title : titles.entrySet()) {
                document.addField(title.getKey(), title.getValue());
            }
        }

        if (!titles.isEmpty()) {
            for (Map.Entry<String, Place> place : places.entrySet()) {
                document.addField(place.getKey(), place.getValue());
            }
        }*/

        if (this.getWorkId() != null && !(this.getWorkId().isEmpty())){
            System.out.println("znotraj pojavne oblike 1");
            document.addField("workId", workId);
        }

        if(this.getCobissId() != null && !(this.getCobissId().isEmpty())){
            System.out.println("znotraj pojavne oblike 2");
            document.addField("cobissId", cobissId);
        }

        if (this.getOtherAuthors() !=null && !(this.getOtherAuthors().isEmpty())){
            document.addField("otherAuthors", otherAuthors);
        }

        if (this.getSubjectHeadings() !=null && !(this.getSubjectHeadings().isEmpty())){
            document.addField("subjectHeadings", subjectHeadings);
        }

        if (this.getNotes() !=null && !(this.getNotes().isEmpty())){
            document.addField("notes", notes);
        }

        if (this.getUDK() !=null && !(this.getUDK().isEmpty())){
            document.addField("UDK", UDK);
        }

        if (this.getISBN() !=null && !(this.getISBN().isEmpty())){
            document.addField("ISBN", ISBN);
        }

        if (this.getCobissTitle() !=null && !(this.getCobissTitle().isEmpty())){
            document.addField("cobissTitle", cobissTitle);
        }

        return document;
    }

}
