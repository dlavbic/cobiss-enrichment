package net.lavbic.web;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.ConsoleHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebClientOptions;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlDivision;
import com.gargoylesoftware.htmlunit.html.HtmlPage;


public class Parser {
	
	private WebClient wc;
	private Logger log;
	
	
	public static void main(String[] args) {
		Parser p = new Parser();
		p.parseHise();
	}
	
	
	public Parser() {
		wc = new WebClient();
		WebClientOptions opts = wc.getOptions(); 
		opts.setJavaScriptEnabled(false);
		opts.setCssEnabled(false);
		
		log = Logger.getLogger(this.getClass().getName());
		
		ConsoleHandler ch = new ConsoleHandler();
		ch.setFormatter(new LogFormatter());
		log.addHandler(ch);
		ch.setLevel(Level.ALL);
		log.setLevel(Level.ALL);
		log.setUseParentHandlers(false);
	}
	
	
	@SuppressWarnings("unchecked")
	protected void parseHise() {
		try {
			HtmlDivision opis_oglasa;
			String URL = "http://www.nepremicnine.net/nepremicnine.html?d=197&p=1&n=2&r=5&pg=1";
			int stOglasov = 0;
			do {
				log.info("Dostop do spletne strani " + URL + ".");
				HtmlPage p = wc.getPage(URL);
				List<HtmlDivision> oglasi = (List<HtmlDivision>) p.getByXPath("//div[contains(@class, 'oglas_container')]");
				for (HtmlDivision oglas : oglasi) {
					stOglasov++;
					opis_oglasa = oglas.getFirstByXPath("./div[@class='teksti_container']");
					log.info("Oglas (" + 
							"ID = " + oglas.getAttribute("id") + ", " + 
							"lokacija = '" + ((HtmlAnchor)opis_oglasa.getFirstByXPath("./h2/a")).asText() + "', " + 
							"povezava = '" + "http://www.nepremicnine.net" + ((HtmlAnchor)opis_oglasa.getFirstByXPath("./h2/a")).getHrefAttribute() + "'");
				}
				URL = (p.getFirstByXPath("//a[@class='next']") == null) ? "" : 
					"http://www.nepremicnine.net" + ((HtmlAnchor)p.getFirstByXPath("//a[@class='next']")).getHrefAttribute();
			} while (URL.length() > 0);
			log.info("Najdenih " + stOglasov + " oglasov.");
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
	}
	
}


class LogFormatter extends Formatter {
	
    private static final DateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss.SSS");
 
    @Override
    public String format(LogRecord record) {
        StringBuilder builder = new StringBuilder(1000);
        builder.append(df.format(new Date(record.getMillis()))).append(" - ");
        builder.append("[").append(record.getSourceClassName()).append(".");
        builder.append(record.getSourceMethodName()).append("] - ");
        builder.append("[").append(record.getLevel()).append("] - ");
        builder.append(formatMessage(record));
        builder.append("\n");
        return builder.toString();
    }
 
    public String getHead(Handler h) {
        return super.getHead(h);
    }
 
    public String getTail(Handler h) {
        return super.getTail(h);
    }

}