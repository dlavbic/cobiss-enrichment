package net.lavbic.web;

import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;


@SuppressWarnings("deprecation")
public class Client {
	
	private DefaultHttpClient httpClient;
	private String charSet;
	private boolean debug;
	private boolean acceptAllSSL;
//	private String localRepository;
	
	
	public Client() {
		//httpClient = new DefaultHttpClient();	    
		
		httpClient = new DefaultHttpClient(new ThreadSafeClientConnManager());
	    charSet = "UTF-8";
	    acceptAllSSL = false;
//	    localRepository = null;
	    debug = false;
	}
	
	
	public Client(boolean acceptAllSSL, String charSet) {
		this();
		this.acceptAllSSL = acceptAllSSL;
		this.charSet = charSet;
		if (acceptAllSSL)
			setSSLAcceptAll();
	}
	
	
	public void setDebug(boolean debug) {
		this.debug = debug;
	}
	
	
	/**
	 * Set user agent (if not specified (null), the default Firefox 3.6.2 is used
	 * 
	 * @param userAgent name of user agent
	 */
	public void setUserAgent(String userAgent) {
		if (userAgent == null)
			httpClient.getParams().setParameter(HttpMethodParams.USER_AGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2");
		else
			httpClient.getParams().setParameter(HttpMethodParams.USER_AGENT, userAgent);
	}
	
	
	/**
	 * Set proxy server
	 * 
	 * @param server address (e.g. 127.0.0.1)
	 * @param port (e.g. 8080)
	 * @param protocol (e.g. http)
	 */
	public void setProxy(String server, int port, String protocol) {		
		httpClient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, new HttpHost(server, port, protocol));
	}
	
	
//	public void setLocalRepository(String folder) {
//		localRepository = folder;
//	}
	
	
	public String get(String getString) {
		String result = "";
		try {
//			boolean useCache = (localRepository != null && new File(localRepository + getString.split("//")[1]).exists()) ? true : false;
//			if (useCache) {
//				if (debug) System.out.print("GET  : [cache] " + getString);
//				result = FileUtils.readFileToString(new File(localRepository + getString.split("//")[1]), "UTF-8");
//				if (debug) System.out.println(" (" + "OK" + ")");
//			} else {
				if (debug) System.out.print("GET  : " + getString);
				HttpResponse response = httpClient.execute(new HttpGet(getString));
				if (debug) System.out.println(" (" + response.getStatusLine() + ")");
				result = EntityUtils.toString(response.getEntity(), charSet);
//				if (localRepository != null)
//					FileUtils.writeStringToFile(new File(localRepository + getString.split("//")[1]), result, "UTF-8");
//			}
		} catch (Exception e) {
			if (debug) System.err.println("Error performing GET reguest '" + getString + "'");
			e.printStackTrace();
			//System.exit(0);
		}
		return result;
	}
		
	
	public String post(String postString, String[][] params, File[] files) {
		String result = "";
		try {
//			String cacheFilename = localRepository + postString.split("//")[1];

			HttpPost httpPost = new HttpPost(postString);
			
			/* Set parameters */
			if (params != null && params.length > 0) {
				if (files != null) {
					if (files.length != params.length)
						throw new Exception("Parameters size don't match!");
					MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
					for (int i=0; i < files.length; i++) {
						if (files[i] == null)
							entity.addPart(params[i][0], new StringBody(params[i][1]));
						else
							entity.addPart(params[i][0], new FileBody(files[i], params[i][1]));
					}
					httpPost.setEntity(entity);
				} else {
					List <NameValuePair> nvps = new ArrayList <NameValuePair>();
					for (int i=0; i < params.length; i++) {
						nvps.add(new BasicNameValuePair(params[i][0], params[i][1]));
//						cacheFilename += "&" + params[i][0] + "=" + params[i][1];
					}
					httpPost.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));
				}
			}
			
//			boolean useCache = (localRepository != null && new File(cacheFilename).exists()) ? true : false;			
//			if (useCache) {
//				if (debug) System.out.print("POST : [cache] " + postString);
//				result = FileUtils.readFileToString(new File(cacheFilename), "UTF-8");
//				if (debug) System.out.println(" (" + "OK" + ")");
//			} else {
				if (debug) System.out.print("POST : " + postString);
				/* Execute the request */
				HttpResponse response = httpClient.execute(httpPost);
				if (debug) System.out.println(" (" + response.getStatusLine() + ")");
				
				/* Check for redirects */
				String redirectUrl = null;
				if (response.containsHeader("Location")) {
					int lastSlash = postString.lastIndexOf("/") == postString.indexOf("/") + 1 ? postString.length() : postString.lastIndexOf("/");
					redirectUrl = postString.subSequence(0, lastSlash) + "/" + response.getFirstHeader("Location").getValue();
				}
				
				/* Get the content */
				HttpEntity entity = response.getEntity();
				result = EntityUtils.toString(entity, charSet);
				
//				if (localRepository != null)
//					FileUtils.writeStringToFile(new File(cacheFilename), result, "UTF-8");
				
				if (redirectUrl != null)
					get(redirectUrl);
//			}
		} catch (Exception e) {
			if (debug) System.err.println("Error performing POST reguest '" + postString + "'");
			e.printStackTrace();
			//System.exit(0);
		}
		return result;		
	}
	
	
	public String post(String postString, String[][] params) {
		String result = "";
		try {
//			String cacheFilename = localRepository + postString.split("//")[1];

			HttpPost httpPost = new HttpPost(postString);
			
			/* Set parameters */
			if (params != null && params.length > 0) {
				List <NameValuePair> nvps = new ArrayList <NameValuePair>();
				for (int i=0; i < params.length; i++) {
					nvps.add(new BasicNameValuePair(params[i][0], params[i][1]));
//					cacheFilename += "&" + params[i][0] + "=" + params[i][1];
				}
				httpPost.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));
			}
			
//			boolean useCache = (localRepository != null && new File(cacheFilename).exists()) ? true : false;			
//			if (useCache) {
//				if (debug) System.out.print("POST : [cache] " + postString);
//				result = FileUtils.readFileToString(new File(cacheFilename), "UTF-8");
//				if (debug) System.out.println(" (" + "OK" + ")");
//			} else {
				if (debug) System.out.print("POST : " + postString);
				/* Execute the request */
				HttpResponse response = httpClient.execute(httpPost);
				if (debug) System.out.println(" (" + response.getStatusLine() + ")");
				
				/* Check for redirects */
				String redirectUrl = null;
				if (response.containsHeader("Location")) {
					int lastSlash = postString.lastIndexOf("/") == postString.indexOf("/") + 1 ? postString.length() : postString.lastIndexOf("/");
					redirectUrl = postString.subSequence(0, lastSlash) + "/" + response.getFirstHeader("Location").getValue();
				}
				
				/* Get the content */
				HttpEntity entity = response.getEntity();
				result = EntityUtils.toString(entity, charSet);
				
//				if (localRepository != null)
//					FileUtils.writeStringToFile(new File(cacheFilename), result, "UTF-8");
				
				if (redirectUrl != null)
					get(redirectUrl);
//			}
		} catch (Exception e) {
			if (debug) System.err.println("Error performing POST reguest '" + postString + "'");
			e.printStackTrace();
			//System.exit(0);
		}
		return result;
	}
	
	
	public void shutDown() {
		httpClient.getConnectionManager().shutdown();
	}
	
	
	private void setSSLAcceptAll() {
		try {
			SSLContext ctx = SSLContext.getInstance("TLS");
			X509TrustManager tm = new X509TrustManager() {
				public void checkClientTrusted(X509Certificate[] xcs, String string) throws CertificateException {}
				public void checkServerTrusted(X509Certificate[] xcs, String string) throws CertificateException {}
				public X509Certificate[] getAcceptedIssuers() { return null; }
			};
			ctx.init(null, new TrustManager[] { tm }, null);
			
			SSLSocketFactory ssf = new SSLSocketFactory(ctx);			
			ssf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			
			ClientConnectionManager ccm = httpClient.getConnectionManager();
			
			SchemeRegistry sr = ccm.getSchemeRegistry();
			sr.register(new Scheme("https", ssf, 443));
			
			httpClient = new DefaultHttpClient(ccm, httpClient.getParams());
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	
	public void setClientCertificate(String certFile, String certPass) {
		try {
			SSLContext ctx = SSLContext.getInstance("TLS");

			KeyStore ks = KeyStore.getInstance("PKCS12");  
			ks.load(new FileInputStream(certFile), certPass.toCharArray());
			
			KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
			kmf.init(ks, certPass.toCharArray());
			
			if (acceptAllSSL) {
				X509TrustManager tm = new X509TrustManager() {
					public void checkClientTrusted(X509Certificate[] xcs, String string) throws CertificateException {}
					public void checkServerTrusted(X509Certificate[] xcs, String string) throws CertificateException {}
					public X509Certificate[] getAcceptedIssuers() { return null; }
				};
				ctx.init(kmf.getKeyManagers(), new TrustManager[] { tm }, new SecureRandom());
			} else {
				ctx.init(kmf.getKeyManagers(), null, new SecureRandom());
			}
			
			SSLSocketFactory ssf = new SSLSocketFactory(ctx);
			ssf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			
			ClientConnectionManager ccm = httpClient.getConnectionManager();
			
			SchemeRegistry sr = ccm.getSchemeRegistry();
			sr.register(new Scheme("https", ssf, 443));
			
			httpClient = new DefaultHttpClient(ccm, httpClient.getParams());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}