<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<title>Obogatitev podatkov sistema Cobiss</title>
</head>
<body>
<div class="container">
<h1>Išči dela:</h1>
<br />
<div class="row">
<a href="/admin">Admin</a>
</div>
<br />
<div class="row">
<form:form class="form-inline" method="post" action="authorSearch">
  <div class="form-group">
    <div class="col-sm-4">
        <label for="inputAuthor" class="control-label">določenega avtorja</label>
    </div>
    <div class="col-sm-8">
        <input name="inputAuthor" class="form-control" id="inputAuthor" placeholder="Author" />
    </div>
  </div>
  <button type="submit" class="btn btn-default">Išči</button>
</form:form>
<br />
<form class="form-inline" method="post" action="titleSearch" >
  <div class="form-group">
    <div class="col-sm-4">
       <label for="inputTitle" class="control-label">po naslovu / poimenovanju</label>
    </div>
    <div class="col-sm-8">
        <input name="inputTitle" class="form-control" id="inputTitle" placeholder="Name">
    </div>
  </div>
  <button type="submit" class="btn btn-default">Išči</button>
</form>
<br />
<form class="form-inline">
  <div class="form-group">
    <label for="inputContent" class="col-sm-4 control-label">po vsebini</label>
    <div class="col-sm-8">
        <input name="inputContent" class="form-control" id="inputContent" placeholder="Content">
    </div>
  </div>
  <button type="submit" class="btn btn-default">Išči</button>
</form>
<br />
<form class="form-inline">
  <div class="form-group">
    <label for="inputKeywords" class="col-sm-4 control-label">po ključnih besedah/vseh poljih</label>
    <div class="col-sm-8">
        <input type="content" class="form-control" id="inputKeywords" placeholder="Keywords">
    </div>
  </div>
  <button type="submit" class="btn btn-default">Išči</button>
</form>
</div>
</div>
</body>
</html>