package main.java.controllers;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

/**
 * Created by viktorharej on 22.02.16.
 */
@Controller
public class Admin {

    @RequestMapping("/admin")
    public String admin(Map<String, Object> model) throws IOException, SolrServerException {

        String urlString = "http://localhost:8983/solr/cobiss";
        SolrClient solr = new HttpSolrClient(urlString);

        SolrQuery query = new SolrQuery();
        query.setSort("timestamp", SolrQuery.ORDER.desc);

        QueryResponse response = solr.query(query);
        SolrDocumentList list = response.getResults();

        ArrayList<String> manIds = new ArrayList<String>();
        //ArrayList<String> workIds = new ArrayList<String>();

        for (SolrDocument sD: list){

            String id = (String) sD.getFirstValue("cobissId");
            if (id.length()>7){
                manIds.add(id);
            }

        }

        int count = list.size();
        System.out.println("Kount: " + count);
        model.put("count", count);
        model.put("manIds", manIds);
        return "admin";
    }


}
