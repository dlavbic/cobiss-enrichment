package main.java.controllers;

import com.viktorharej.cobiss.Scraper;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


import javax.servlet.http.HttpServletRequest;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.Map;
import com.viktorharej.cobiss.*;
/**
 * Created by viktorharej on 18.01.16.
 */
@Controller
public class Search {

    @RequestMapping(value="/authorSearch", method = RequestMethod.POST)
    public String authorSearch(@RequestParam("inputAuthor") String input, Model model) throws IOException, SolrServerException, XPathExpressionException {
        //1. step parse query
        Scraper.parseContributor(input);


        //System.out.println(list);

        //2. step send query to parse records
        //3. step after parsing records is done do the solr search
        //4. get and return results

        return "results";
    }

    @RequestMapping(value="/contentSearch", method = RequestMethod.POST)
    public String contentSearch(Map<String, Object> model) {
        return "results";
    }

    @RequestMapping(value="/titleSearch", method = RequestMethod.POST)
    public String titletSearch(@RequestParam("inputTitle") String input, Model model) throws IOException, SolrServerException, XPathExpressionException, UnsupportedEncodingException {
        Scraper.parseTitle(input);

        String urlString = "http://localhost:8983/solr/cobiss";
        SolrClient solr = new HttpSolrClient(urlString);

        SolrQuery query = new SolrQuery();
        System.out.println("Iskanje...");
        String encodedQuery = URLEncoder.encode(input, "UTF-8");
        query.set(encodedQuery, "contributor");
        QueryResponse response = solr.query(query);
        SolrDocumentList list = response.getResults();
        String string = list.toString();


        model.addAttribute("test", string);
        return "results";
    }

}
